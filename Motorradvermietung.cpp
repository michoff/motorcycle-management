ÿþ# i n c l u d e   " p c h . h "  
 # i n c l u d e   < c s t d l i b >  
 # i n c l u d e   < s t r i n g >  
 # i n c l u d e   < i o s t r e a m >  
 # i n c l u d e   < l i s t >  
 # i n c l u d e   < s s t r e a m >  
 # i n c l u d e   < f s t r e a m >  
 u s i n g   n a m e s p a c e   s t d ;  
  
 / * B e i   d e n   g a n z e n   K l a s s e n   w u r d e   e s   u n u e b e r s i c h t l i c h   i n   d e r   D a t e i ,   d a h e r   b e l a s s e   i c h   d i e   * - T r e n n e r   z u r   a n g e n e h m e r e n   K o n t r o l l e * /  
  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / /   S T R U C T U R E :   M O T O R R A D  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / * D i e n t   a l s   G e r u e s t   f u e r   e i n   M o t o r r a d . * /  
 s t r u c t   M o t o r r a d   / /   D e f i n i e r e   d e n   T y p  
 {  
 	 s t r i n g   m o d e l l ;   / / M o t o r r a d m o d e l l n a m e  
 	 s t r i n g   s t a t u s ;   / / w i r d   " g e l a g e r t   /   v e r l i e h e n   /   r e s e r v i e r t "  
 	 s t r i n g   k u n d e n v o r n a m e ;   / / f a l l s   v e r l i e h e n   o d e r   r e s e r v i e r t   i s t   h i e r   d e r   n a m e   d e s   K u n d e n  
 	 s t r i n g   k u n d e n n a c h n a m e ;   / / f a l l s   v e r l i e h e n   o d e r   r e s e r v i e r t   i s t   h i e r   d e r   n a m e   d e s   K u n d e n  
 	 b o o l   o p e r a t o r   = =   ( c o n s t   M o t o r r a d &   k )   c o n s t   {   r e t u r n   m o d e l l   = =   k . m o d e l l ;   }  
 	 b o o l   o p e r a t o r   ! =   ( c o n s t   M o t o r r a d &   k )   c o n s t   {   r e t u r n   ! o p e r a t o r = = ( k ) ;   }  
 } ;  
  
 / * V e r w e n d e t   d e n   A u s g a b e s t r o m   u m   e i n   M o t o r r a d   u n d   s e i n e   r e l e v a n t e n   I n f o r m a t i o n e n   a n z u z e i g e n * /  
 o s t r e a m   &   o p e r a t o r < < ( o s t r e a m   &   o s ,   c o n s t   M o t o r r a d   &   r )  
 {  
 	 o s   < <   " M o t o r r a d :   "   < <   r . m o d e l l   < <   e n d l  
 	 	 < <   " S t a t u s :   "   < <   r . s t a t u s   < <   e n d l ;  
 	 i f   ( r . s t a t u s   = =   " v e r l i e h e n "   | |   r . s t a t u s   = =   " r e s e r v i e r t " )   {  
 	 	 o s   < <   " K u n d e :   "   < <   r . k u n d e n v o r n a m e   < <   "   "   < <   r . k u n d e n n a c h n a m e   < <   e n d l ;  
 	 }  
 	 r e t u r n   o s ;  
 }  
  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / /   N E X T   C L A S S :   A D R E S S E  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / * R e p r ä s e n t i e r t   d i e   A d r e s s e   e i n e s   K u n d e n * /  
 c l a s s   A d r e s s e  
 {  
 	 f r i e n d   c l a s s   K u n d e ;  
 p u b l i c :  
 	 A d r e s s e ( )   { }  
 	 A d r e s s e ( s t r i n g   s t r ,   i n t   n r ,   i n t   p l z ,   s t r i n g   o r t )   :   s t r a s s e ( s t r ) ,   n r ( n r ) ,   p l z ( p l z ) ,   o r t ( o r t )   { }  
 	 f r i e n d   o s t r e a m &   o p e r a t o r < <   ( o s t r e a m &   o s ,   c o n s t   A d r e s s e &   a ) ;  
 	 f r i e n d   o f s t r e a m   &   o p e r a t o r < < ( o f s t r e a m   &   o f ,   c o n s t   A d r e s s e   &   a ) ;  
 	 f r i e n d   i s t r e a m &   o p e r a t o r > >   ( i s t r e a m &   i s ,   A d r e s s e &   a ) ;  
 p r i v a t e :  
 	 s t r i n g   s t r a s s e ,   o r t ;  
 	 i n t   n r ,   p l z ;  
 } ;  
  
 / * V e r w e n d e t   d e n   A u s g a b e s t r o m ,   u m   e i n e   A d r e s s e   a n z u z e i g e n * /  
 o s t r e a m   &   o p e r a t o r < < ( o s t r e a m   &   o s ,   c o n s t   A d r e s s e   &   a )  
 {  
 	 o s   < <   " S t r a s s e :   "   < <   a . s t r a s s e   < <   "   "   < <   a . n r   < <   e n d l  
 	 	 < <   " O r t :   "   < <   a . p l z   < <   "   "   < <   a . o r t ;  
 	 r e t u r n   o s ;  
 }  
  
 / * A u s g a b e   i n   d i e   D a t e i   r e s e r v i e r u n g e n . t x t * /  
 o f s t r e a m   &   o p e r a t o r < < ( o f s t r e a m   &   o f ,   c o n s t   A d r e s s e   &   a )  
 {  
 	 o f   < <   a . s t r a s s e   < <   "   "   < <   a . n r   < <   "   "   < <   a . p l z   < <   "   "   < <   a . o r t ;  
 	 r e t u r n   o f ;  
 }  
  
 / * V e r w e n d e t   d e n   E i n g a b e s t r o m ,   u m   e i n e   A d r e s s e   a n z u l e g e n * /  
 i s t r e a m   &   o p e r a t o r > > ( i s t r e a m &   i s ,   A d r e s s e   &   a )  
 {  
 	 s t d : : c o u t   < <   e n d l   < <   " S t r a s s e   ( o h n e   H a u s n u m m e r ) :   " ;  
 	 i s   > >   a . s t r a s s e ;  
 	 s t d : : c o u t   < <   " H a u s n u m m e r :   " ;  
 	 i s   > >   a . n r ;  
 	 s t d : : c o u t   < <   " P L Z :   " ;  
 	 i s   > >   a . p l z ;  
 	 s t d : : c o u t   < <   " O r t :   " ;  
 	 i s   > >   a . o r t ;  
 	 r e t u r n   i s ;  
 }  
  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / /   N E X T   C L A S S   :   K U N D E  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
  
 / * R e p r ä s e n t i e r t   e i n e n   K u n d e n   d u r c h   s e i n e   D a t e n   u n d   g g f .   I n f o r m a t i o n e n   z u   v o n   i h m   r e s e r v i e r t e n   /   g e l i e h e n e n   M o t o r r a e d e r n . * /  
 c l a s s   K u n d e  
 {  
 	 f r i e n d   c l a s s   K u n d e n L i s t e ;  
 	 f r i e n d   c l a s s   R e s e r v i e r u n g s L i s t e ;  
 p u b l i c :  
 	 / / k o n s t r u k t o r e n  
 	 K u n d e ( )   :   n a m e ( "   " ) ,   v o r n a m e ( "   " ) ,   t e l e f o n ( "   " ) ,   g e b J a h r ( 0 ) ,   f u e h r e r s c h e i n ( f a l s e ) ,   a d r e s s e ( "   " ,   0 ,   0 ,   "   " ) ,   s t a t u s ( " 0 " )   { }  
 	 K u n d e ( s t r i n g   n n ,   s t r i n g   v n ,   s t r i n g   t e l ,   i n t   g e b ,   A d r e s s e   a d r e s s e ,   b o o l   f u e h e r s c h e i n )   :    
 	 	 n a m e ( n n ) ,  
 	 	 v o r n a m e ( v n ) ,  
 	 	 t e l e f o n ( t e l ) ,  
 	 	 g e b J a h r ( g e b ) ,  
 	 	 a d r e s s e ( a d r e s s e ) ,  
 	 	 f u e h r e r s c h e i n ( f u e h e r s c h e i n ) ,  
 	 	 s t a t u s ( " 0 " )  
 	 { } ;  
 	 / / o p e r a t o r e n  
 	 f r i e n d   o f s t r e a m &   o p e r a t o r   < <   ( o f s t r e a m &   o f ,   c o n s t   K u n d e &   k ) ;  
 	 f r i e n d   o s t r e a m &   o p e r a t o r < <   ( o s t r e a m &   o s ,   c o n s t   K u n d e &   k ) ;  
 	 f r i e n d   i s t r e a m &   o p e r a t o r > >   ( i s t r e a m &   i s ,   K u n d e &   k ) ;  
 	 b o o l   o p e r a t o r   = =   ( c o n s t   K u n d e &   k )   c o n s t   {   r e t u r n   n a m e   = =   k . n a m e   & &   v o r n a m e   = =   k . v o r n a m e ;   }   / / o p e r a t o r   f o r   l i s t . r e m o v e ( )  
 	 b o o l   o p e r a t o r   ! =   ( c o n s t   K u n d e &   k )   c o n s t   {   r e t u r n   ! o p e r a t o r = = ( k ) ;   }   / / o p e r a t o r   f o r   l i s t . r e m o v e ( )  
 p r i v a t e :  
 	 / / a t t r i b u t e  
 	 s t r i n g   n a m e ,   v o r n a m e ,   t e l e f o n ,   s t a t u s ;  
 	 i n t   g e b J a h r ;  
 	 A d r e s s e   a d r e s s e ;  
 	 b o o l   f u e h r e r s c h e i n ;  
 	 M o t o r r a d   a s s o z i i e r t e s M o t o r r a d ;  
 	 / / m e t h o d e n  
 	 s t r i n g   h a t F u e h r e r s c h e i n ( )   c o n s t ;  
 	  
 } ;  
  
 / * V e r w e n d e t   d e n   A u s g a b e s t r o m   u m   e i n   K u n d e n   a n z u z e i g e n * /  
 o s t r e a m   &   o p e r a t o r < < ( o s t r e a m   &   o s ,   c o n s t   K u n d e   &   k )  
 {  
 	 o s   < <   " * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * "   < <   e n d l  
 	 	 < <   " N a m e :   "   < <   k . v o r n a m e   < <   "   "   < <   k . n a m e   < <   e n d l  
 	 	 < <   " G e b u r t s j a h r   :   "   < <   k . g e b J a h r   < <   e n d l  
 	 	 < <   " A d r e s s e :   "   < <   e n d l   < <   k . a d r e s s e   < <   e n d l  
 	 	 < <   " T e l e f o n :   "   < <   k . t e l e f o n   < <   e n d l  
 	 	 < <   " F u e h r e r s c h e i n   K l a s s e   A   v o r h a n d e n :   "   < <   k . h a t F u e h r e r s c h e i n ( )   < <   e n d l ;  
 	 / / T o D o :   r e s e r v i e r u n g   c h e c k   - >   a u s g a b e   m o t o r r a d  
 	 i f   ( k . s t a t u s   = =   " r e s e r v i e r t " )   {  
 	 	 o s   < <   " R e s e r v i e r t e s   M o t o r r a d :   "   < <   k . a s s o z i i e r t e s M o t o r r a d . m o d e l l   < <   e n d l ;  
 	 }  
 	 i f   ( k . s t a t u s   = =   " a u s g e l i e h e n " )   {  
 	 	 o s   < <   " A u s g e l i e h e n e s   /   a u s g e l a g e r t e s   M o t o r r a d :   "   < <   k . a s s o z i i e r t e s M o t o r r a d . m o d e l l   < <   e n d l ;  
 	 }  
 	 o s   < <   " * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * "   < <   e n d l ;  
 	 r e t u r n   o s ;  
 }  
  
 / * A u s g a b e   i n   d i e   D a t e i   r e s e r v i e r u n g e n . t x t * /  
 o f s t r e a m   &   o p e r a t o r < < ( o f s t r e a m   &   o f ,   c o n s t   K u n d e   &   k )  
 {  
 	 o f   < <   k . n a m e   < <   "   "   < <   k . v o r n a m e   < <   "   "   < <   k . g e b J a h r   < <   "   "   < <   k . t e l e f o n   < <   "   "   < <   k . h a t F u e h r e r s c h e i n ( )   < <   "   "    
 	 	 < <   k . a s s o z i i e r t e s M o t o r r a d . m o d e l l   < <   "   " ;  
 	 o f   < <   k . a d r e s s e   < <   e n d l ;  
 	 r e t u r n   o f ;  
 }  
  
 / * B e n u t z t   d e n   E i n g a b e s t r o m ,   u m   e i n e n   K u n d e n   z u   e r z e u g e n . * /  
 i s t r e a m   &   o p e r a t o r > > ( i s t r e a m   &   i s ,   K u n d e   &   k )  
 {  
 	 / / K u n d e ( s t r i n g   n n ,   s t r i n g   v n ,   s t r i n g   t e l ,   i n t   g e b ,   A d r e s s e   a d r e s s e ,   s t r i n g   f u e h e r s c h e i n )  
 	 s t d : : c o u t   < <   e n d l   < <   " N a c h n a m e :   " ;  
 	 i s   > >   k . n a m e ;  
 	 s t d : : c o u t   < <   " V o r n a m e :   " ;  
 	 i s   > >   k . v o r n a m e ;  
 	 s t d : : c o u t   < <   " T e l e f o n n u m m e r :   " ;  
 	 i s   > >   k . t e l e f o n ;  
 	 s t d : : c o u t   < <   " G e b u r t s j a h r :   " ;  
 	 i s   > >   k . g e b J a h r ;  
 	 s t d : : c o u t   < <   " A d r e s s e :   " ;  
 	 i s   > >   k . a d r e s s e ;  
 	 s t d : : c o u t   < <   " I s t   d e r   F u e h e r s c h e i n   K l a s s e   A   v o r h a n d e n ?   ( j / n ) " ;  
 	 c h a r   c h e c k ;  
 	 i s   > >   c h e c k ;  
 	 i f   ( c h e c k   = =   ' J '   | |   c h e c k   = =   ' j ' )  
 	 {  
 	 	 k . f u e h r e r s c h e i n   =   t r u e ;  
 	 }  
 	 r e t u r n   i s ;  
 }  
  
 / * G i b t   t e x t u e l l   a n ,   o b   d e r   K u n d e   e i n e n   F u e h e r s c h e i n   K l a s s e   A   b e s i t z t . * /  
 s t r i n g   K u n d e : : h a t F u e h r e r s c h e i n ( )   c o n s t  
 {  
 	 r e t u r n   f u e h r e r s c h e i n   ?   " J a "   :   " N e i n " ;  
 }  
  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / /   N E X T   C L A S S   :   K U N D E N L I S T E  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / * s t e l l t   e i n e   L i s t e   f u e r   K u n d e n d a t e n   b e r e i t   u n d   b i e t e t   d i v e r s e   F u n k t i o n e n   u m   d i e   L i s t e   z u   v e r a e n d e r n   o d e r   a u s z u w e r t e n . * /  
 c l a s s   K u n d e n L i s t e  
 {  
 p u b l i c :  
 	 K u n d e n L i s t e ( )   { } ;  
 	 v o i d   a d d K u n d e ( ) ;  
 	 v o i d   a d d K u n d e ( K u n d e   k ) ;  
 	 v o i d   r e m o v e K u n d e ( s t r i n g   n a m e ,   s t r i n g   v o r n a m e ) ;  
 	 v o i d   r e m o v e K u n d e ( K u n d e   k ) ;  
 	 v o i d   p r i n t K u n d e n ( ) ;  
 	 K u n d e   g e t K u n d e ( s t r i n g   n a c h n a m e ,   s t r i n g   v o r n a m e ) ;  
 	 b o o l   k u n d e I s t A n g e l e g t ( s t r i n g   n a c h n a m e ,   s t r i n g   v o r n a m e )   c o n s t ;  
 	 v o i d   a e n d e r K u n d e n S t a t u s ( K u n d e   k ) ;  
 p r i v a t e :  
 	 l i s t < K u n d e >   k u n d e n ;  
 } ;  
 / * E r s t e l l t   a u f   G r u n d l a g e   e i n e r   N u t z e r e i n g a b e   e i n e n   n e u e n   K u n d e n . * /  
 v o i d   K u n d e n L i s t e : : a d d K u n d e ( )  
 {  
 	 K u n d e   k ;  
 	 s t d : : c i n   > >   k ;  
 	 t h i s - > k u n d e n . p u s h _ b a c k ( k ) ;  
 	 s t d : : c o u t   < <   " D e r   K u n d e   w u r d e   h i n z u g e f u e g t ! "   < <   e n d l ;  
 }  
  
 / * F u e g t   e i n   K u n d e n o b j e k t   i n   d i e   K u n d e n l i s t e   e i n . * /  
 v o i d   K u n d e n L i s t e : : a d d K u n d e ( K u n d e   k )  
 {  
 	 t h i s - > k u n d e n . p u s h _ b a c k ( k ) ;  
 }  
  
 / * E n t f e r n t   e i n e n   K u n d e n   m i t   H i l f e   d e r   N u t z e r e i n g a b e   a u s   d e r   K u n d e n L i s t e * /  
 v o i d   K u n d e n L i s t e : : r e m o v e K u n d e ( s t r i n g   n a m e ,   s t r i n g   v o r n a m e )  
 {  
 	 f o r   ( K u n d e   t m p   :   t h i s - > k u n d e n )  
 	 {  
 	 	 i f   ( t m p . n a m e   = =   n a m e   & &   t m p . v o r n a m e   = =   v o r n a m e )  
 	 	 {  
 	 	 	 t h i s - > k u n d e n . r e m o v e ( t m p ) ;  
 	 	 	 s t d : : c o u t   < <   " F o l g e n d e r   K u n d e   w u r d e   g e l o e s c h t :   "   < <   t m p   < <   e n d l ;  
 	 	 	 r e t u r n ;   / / b e e n d e t   l o o p  
 	 	 }   / /   i f  
 	 }   / /   f o r    
 	 s t d : : c o u t   < <   " D i e s e r   K u n d e   i s t   n i c h t   v o r h a n d e n ,   N a m e :   "   < <   v o r n a m e   < <   "   "   < <   n a m e   < <   e n d l ;  
 }  
  
 / * A u f g r u n d l a g e   e i n e s   v o r h a n d e n e n   K u n d e n   w i r d   e i n   g l e i c h w e r t i g e s   K u n d e n o b j e k t   a u s   d e r   K u n d e n L i s t e   g e l o e s c h t . * /  
 v o i d   K u n d e n L i s t e : : r e m o v e K u n d e ( K u n d e   k )  
 {  
 	 f o r   ( K u n d e   t m p   :   t h i s - > k u n d e n )  
 	 {  
 	 	 i f   ( t m p . n a m e   = =   k . n a m e   & &   t m p . v o r n a m e   = =   k . v o r n a m e )  
 	 	 {  
 	 	 	 t h i s - > k u n d e n . r e m o v e ( t m p ) ;  
 	 	 	 r e t u r n ;   / / b e e n d e t   l o o p .  
 	 	 }   / /   i f  
 	 }   / /   f o r    
 }  
  
 / * G i b t   a l l e   a n g e l e g t e n   K u n d e n   i n   d e r   K o n s o l e   a u s . * /  
 v o i d   K u n d e n L i s t e : : p r i n t K u n d e n ( )  
 {  
 	 s t d : : c o u t   < <   e n d l ;  
 	 f o r   ( K u n d e   k   :   k u n d e n )   {  
 	 	 s t d : : c o u t   < <   k ;  
 	 }  
 }  
  
 / * G i b t   e i n   K u n d e n o b j e k t   a n h a n d   d e s   N a m e n s   w i e d e r ,   s o f e r n   d e r   K u n d e   a n g e l e g t   i s t   * /  
 K u n d e   K u n d e n L i s t e : : g e t K u n d e ( s t r i n g   n a c h n a m e ,   s t r i n g   v o r n a m e )  
 {  
 	 f o r   ( K u n d e   & k   :   k u n d e n )   {  
 	 	 i f   ( k . n a m e   = =   n a c h n a m e   & &   k . v o r n a m e   = =   v o r n a m e )   {  
 	 	 	 r e t u r n   k ;  
 	 	 }  
 	 }  
 }  
  
 / * G i b t   w a h r   z u r u e c k ,   w e n n   d e r   K u n d e   a n g e l e g t   i s t . * /  
 b o o l   K u n d e n L i s t e : : k u n d e I s t A n g e l e g t ( s t r i n g   n a c h n a m e ,   s t r i n g   v o r n a m e )   c o n s t  
 {  
 	 f o r   ( K u n d e   k   :   k u n d e n )   {  
 	 	 i f   ( k . n a m e   = =   n a c h n a m e   & &   k . v o r n a m e   = =   v o r n a m e )   {  
 	 	 	 r e t u r n   t r u e ;  
 	 	 }  
 	 }  
 	 r e t u r n   f a l s e ;  
 }  
  
 / * D i e n t   d e r   A e n d e r u n g   d e s   K u n d e n s t a t u s .   L o e s c h t   d e n   K u n d e n   a n h a n d   d e s   K u n d e n n a m e n   u n d   f u e g t   i n   d e r  
 L i s t e   e i n   n e u e s   K u n d e n o b j e k t   m i t   d e n   g e w u n s c h t e n   D a t e n   e i n * /  
 v o i d   K u n d e n L i s t e : : a e n d e r K u n d e n S t a t u s ( K u n d e   k )  
 {  
 	 r e m o v e K u n d e ( k ) ;  
 	 a d d K u n d e ( k ) ;  
 }  
  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / /   N E X T   C L A S S :   R E S E R V I E R U N G  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / * R e p r ä s e n t i e r t   e i n e   R e s e r v i e r u n g .  
 E i n e   R e s e r v i e r u n g   u m f a s s t   e i n e n   K u n d e n   u n d   d a s   r e s e r v i e r t e   M o t o r r a d .  
 * /  
 c l a s s   R e s e r v i e r u n g  
 {  
 	 f r i e n d   c l a s s   R e s e r v i e r u n g s L i s t e ;  
 p u b l i c :  
 	 R e s e r v i e r u n g ( K u n d e   & k u n d e ,   M o t o r r a d   & m o t o r r a d )   :   k u n d e ( k u n d e ) ,   m ( m o t o r r a d )   { } ;  
 	 f r i e n d   o s t r e a m   &   o p e r a t o r < < ( o s t r e a m   &   o s ,   c o n s t   R e s e r v i e r u n g   &   r ) ;  
 	 f r i e n d   o f s t r e a m   &   o p e r a t o r < < ( o f s t r e a m   &   o f ,   c o n s t   R e s e r v i e r u n g   &   r ) ;  
 	 b o o l   o p e r a t o r   = =   ( c o n s t   R e s e r v i e r u n g &   r )   c o n s t   {   r e t u r n   m   = =   r . m   & &   k u n d e   = =   r . k u n d e ;   }   / / o p e r a t o r   f o r   l i s t . r e m o v e ( )  
 	 b o o l   o p e r a t o r   ! =   ( c o n s t   R e s e r v i e r u n g &   r )   c o n s t   {   r e t u r n   ! o p e r a t o r = = ( r ) ;   }   / / o p e r a t o r   f o r   l i s t . r e m o v e ( )  
 p r i v a t e :  
 	 K u n d e   k u n d e ;  
 	 M o t o r r a d   m ;  
 } ;  
  
 / * D i e n t   d e r   A u s g a b e   i n   d e r   K o n s o l e * /  
 o s t r e a m   &   o p e r a t o r < < ( o s t r e a m   &   o s ,   c o n s t   R e s e r v i e r u n g   &   r )  
 {  
 	 o s   < <   e n d l   < <   " * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * "   < <   e n d l  
 	 	 < <   " K u n d e   d e r   R e s e r v i e r u n g :   "   < <   e n d l   < <   r . k u n d e   < <   e n d l   < <   e n d l ;  
 	 r e t u r n   o s ;  
 }  
  
 / * A u s g a b e   i n   d i e   r e s e r v i e r u n g e n . t x t * /  
 o f s t r e a m   &   o p e r a t o r < < ( o f s t r e a m   &   o f ,   c o n s t   R e s e r v i e r u n g   &   r )  
 {  
 	 o f   < <   r . k u n d e   < <   e n d l   < <   e n d l ;  
 	 r e t u r n   o f ;  
 }  
  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / /   N E X T   C L A S S   :   R E S E R V I E R U N G S L I S T E  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / * R e p r ä s e n t i e r t   e i n e   L i s t e   v o n   R e s e r v i e r u n g e n   u n d   s t e l l t   F u n k t i o n e n   z u r   V e r a e n d e r u n g   d i e s e r   L i s t e   b e r e i t . * /  
 c l a s s   R e s e r v i e r u n g s L i s t e  
 {  
 p u b l i c :  
 	 R e s e r v i e r u n g s L i s t e ( K u n d e n L i s t e   * k l )   :   k u n d e n L i s t e ( k l )   {  
 	 	 / / i n i t i a l i s i e r t   d i e   L i s t e   d e r   M o t o r r a e d e r  
 	 	 m o t o r a e d e r [ 0 ] . m o d e l l   =   " S u z u k i _ B a n d i t " ;  
 	 	 m o t o r a e d e r [ 1 ] . m o d e l l   =   " H o n d a _ T r a n s A l p " ;  
 	 	 m o t o r a e d e r [ 2 ] . m o d e l l   =   " B M W _ F _ 6 5 0 _ G S " ;  
 	 	 m o t o r a e d e r [ 3 ] . m o d e l l   =   " K a w a s a k i _ Z Z R 1 4 0 0 " ;  
 	 	 f o r   ( i n t   i   =   0 ;   i   <   4 ;   i + + )   {  
 	 	 	 m o t o r a e d e r [ i ] . s t a t u s   =   " g e l a g e r t " ;  
 	 	 }  
 	 } ;  
 	 v o i d   a d d R e s e r v i e r u n g ( ) ;  
 	 v o i d   p r i n t R e s e r v i e r u n g e n ( ) ;  
 	 v o i d   p r i n t R e s e r v i e r u n g e n ( l i s t < R e s e r v i e r u n g >   r e s e r ) ;  
 	 v o i d   r e m o v e R e s e r v i e r u n g ( ) ;  
 	 v o i d   a u s l a g e r n D e r R e s e r v i e r u n g ( ) ;  
 	 v o i d   s p e i c h e r n ( ) ;  
 	 v o i d   d a t e i L a d e n ( ) ;  
 	 v o i d   p r i n t M o t o r r a e d e r ( ) ;  
 p r i v a t e :  
 	 l i s t < R e s e r v i e r u n g >   r e s e r v i e r u n g e n ;  
 	 K u n d e n L i s t e   * k u n d e n L i s t e ;  
 	 M o t o r r a d   m o t o r a e d e r [ 4 ] ;  
 	  
 } ;  
  
 / * f u e g t   a n h a n d   d e r   B e n u t z e r e i n g a b e n   e i n e   n e u e   R e s e r v i e r u n g   h i n z u . * /  
 v o i d   R e s e r v i e r u n g s L i s t e : : a d d R e s e r v i e r u n g ( )  
 {  
 	 s t r i n g   v o r n a m e ,   n a c h n a m e ;  
 	 s t d : : c o u t   < <   " F u e r   w e l c h e n   K u n d e n   m o e c h t e n   S i e   e i n e   r e s e r v i e r u n g   v o r n e h m e n ? "   < <   e n d l   < <   " V o r n a m e :   " ;  
 	 s t d : : c i n   > >   v o r n a m e ;  
 	 s t d : : c o u t   < <   e n d l   < <   " N a c h n a m e :   " ;  
 	 s t d : : c i n   > >   n a c h n a m e ;  
 	 K u n d e   k u n ;  
 	 / / w e n n   d e r   K u n d e   n i c h t   a n g e l e g t   i s t ,   e r h a e l t   d e r   B e n u t z e r   d i e   M o e g l i c h t k e i t   e i n e n   N u t z e r   a n z u l e g e n  
 	 i f   ( k u n d e n L i s t e - > k u n d e I s t A n g e l e g t ( n a c h n a m e ,   v o r n a m e ) )   {    
 	 	 k u n   =   k u n d e n L i s t e - > g e t K u n d e ( n a c h n a m e ,   v o r n a m e ) ;  
 	 	 i f   ( ! k u n . f u e h r e r s c h e i n )   {   / / s o f e r n   d e r   K u n d e   k e i n e n   F u e h r e r s c h e i n   h a t ,   d a r f   e r   k e i n   M o t o r r a d   m i e t e n .  
 	 	 	 s t d : : c o u t   < <   " D e r   K u n d e   b e s i t z t   k e i n e n   F u e h r e r s c h e i n   u n d   d a r f   d a h e r   k e i n e   M o t o r r a e d e r   a u s l e i h e n ! "   < <   e n d l ;  
 	 	 	 r e t u r n ;  
 	 	 }  
 	 	 i f   ( k u n . s t a t u s   = =   " a u s g e l i e h e n "   | |   k u n . s t a t u s   = =   " r e s e r v i e r t " )   {   / / a u s g e l i e h e n   =   a u s g e l a g e r t  
 	 	 	 s t d : : c o u t   < <   " D i e s e r   K u n d e   w i r d   b e r e i t s   m i t   e i n e m   M o t o r r a d   a s s o z i i e r t ,   s i e h e   K u n d e n b l a t t :   "   < <   e n d l   < <   e n d l   < <   k u n  
 	 	 	 	 < <   e n d l   < <   e n d l   < <   " D e r   K u n d e   k a n n   n i c h t   m i t   m e h r   a l s   e i n e m   M o t o r r a d   a s s o z i i e r t   w e r d e n ,   d a h e r   w i r d   d i e "  
 	 	 	 	 < <   "   R e s e r v i e r u n g   a n   d i e s e r   S t e l l e   b e e n d e t . "   < <   e n d l   < <   e n d l ;  
 	 	 	 r e t u r n ;   / / D a   d e r   K u n d e   b e r e i t s   e i n   M o t o r r a d   a u s g e l i e h e n   o d e r   r e s e r v i e r t   h a t ,   w i r d   d i e   R e s e r v i e r u n g   b e e n d e t .  
 	 	 }   / / i f  
 	 }   / / i f  
 	 e l s e   {   / / W e n n   d e r   K u n d e   n o c h   n i c h t   a n g e l e g t   i s t ,   e r h a e l t   d e r   U s e r   a n   d i e s e r   S t e l l e   d i e   M o e g l i c h k e i t   d e n   N u t z e r   a n z u l e g e n .  
 	 	 s t d : : c o u t   < <   " D e r   K u n d e   i s t   n i c h t   a n g e l e g t ,   m o e c h t e n   s i e   d e n   K u n d e n   a n l e g e n ?   ( j / n ) "   < <   e n d l ;  
 	 	 c h a r   c h e c k ;  
 	 	 s t d : : c i n   > >   c h e c k ;  
 	 	 i f   ( c h e c k   = =   ' j '   | |   c h e c k   = =   ' J ' )   {  
 	 	 	 k u n d e n L i s t e - > a d d K u n d e ( ) ;   / / B i e t e t   d e m   N u t z e r   d i e   M o e g l i c h k e i t   u e b e r   K o n s o l e n e i n g a b e n   e i n e n   K u n d e n   a n z u l e g e n .  
 	 	 	 k u n   =   k u n d e n L i s t e - > g e t K u n d e ( n a c h n a m e ,   v o r n a m e ) ;  
 	 	 }   / / i f  
 	 	 e l s e   {  
 	 	 	 s t d : : c o u t   < <   " D a   d i e s e   R e s e r v i e r u n g   s o m i t   k e i n e m   K u n d e n   z u g e o r d n e t   w e r d e n   k o e n n t e ,   w i r d   d i e   R e s e r v i e r u n g "  
 	 	 	 	 < <   "   a n   d i e s e r   S t e l l e   b e e n d e t "   < <   e n d l ;  
 	 	 	 r e t u r n ;  
 	 	 }   / / e l s e  
 	 }   / / e l s e  
  
 	 s t d : : c o u t   < <   e n d l   < <   " F o l g e n d e   M o t o r r a e d e r   k o e n n e n   r e s e r v i e r t   w e r d e n : "   < <   e n d l   < <   e n d l ;  
 	 f o r   ( i n t   i   =   0 ;   i   <   4 ;   i + + )   {  
 	 	 i f   ( m o t o r a e d e r [ i ] . s t a t u s   = =   " g e l a g e r t " )   {  
 	 	 	 s t d : : c o u t   < <   m o t o r a e d e r [ i ] ;  
 	 	 }  
 	 }  
 	 s t d : : c o u t   < <   e n d l   < <   e n d l   < <   " B i t t e   g e b e n   s i e   d i e   N u m m e r   d e s   M o t o r r a d s   e i n ,   d a s s   r e s e r v i e r t   w e r d e n   s o l l :   "  
 	 	 < <   e n d l   < <   " 1 :   S u z u k i , "   < <   e n d l    
 	 	 < <   " 2 :   H o n d a , "   < <   e n d l  
 	 	 < <   " 3 :   B M W , "   < <   e n d l  
 	 	 < <   " 4 :   K a w a s a k i "   < <   e n d l ;  
 	 i n t   m o d e l l N r ;  
 	 s t d : : c i n   > >   m o d e l l N r ;  
 	 - - m o d e l l N r ;   / / r e d u z i e r u n g   u m   e i n s ,   d a   d a s   A r r a y   b e i   0   b e g i n n t .  
  
 	 i f ( m o t o r a e d e r [ m o d e l l N r ] . s t a t u s   = =   " v e r l i e h e n " )   {  
 	 	 s t d : : c o u t   < <   " D a s   g e w a e h l t e   M o t o r r a d   i s t   b e r e i t s   v e r l i e h e n ! "  
 	 	 	 < <   e n d l   < <   " S i e h e   M o t o r r a d i n f o b l a t t :   "   < <   e n d l   < <   m o t o r a e d e r [ m o d e l l N r ] ;  
 	 	 r e t u r n ;  
 	 }  
 	 i f   ( m o t o r a e d e r [ m o d e l l N r ] . s t a t u s   = =   " r e s e r v i e r t " )   {  
 	 	 s t d : : c o u t   < <   " D a s   g e w a e h l t e   M o t o r r a d   i s t   b e r e i t s   r e s e r v i e r t ! "  
 	 	 	 < <   e n d l   < <   " S i e h e   M o t o r r a d i n f o b l a t t :   "   < <   e n d l   < <   m o t o r a e d e r [ m o d e l l N r ] ;  
 	 	 r e t u r n ;  
 	 }  
 	 / / s e t z e n   d e r   I n f o   A t t r i b u t e   i m   M o t o r r a d   u n d   i m   K u n d e n .  
 	 k u n . s t a t u s   =   " r e s e r v i e r t " ;  
 	 k u n . a s s o z i i e r t e s M o t o r r a d   =   m o t o r a e d e r [ m o d e l l N r ] ;  
 	 k u n d e n L i s t e - > a e n d e r K u n d e n S t a t u s ( k u n ) ;   / / d e r   K u n d e   w i r d   m i t   s t a t u s   r e s e r v i e r t   h i n z u g e f u e g t   u n d   m i t   d e m   a n d e r e n   S t a t u s   g e l o e s c h t .  
 	 / / i n f o s   i m   M o t o r r a d   D a t e n b l a t t  
 	 m o t o r a e d e r [ m o d e l l N r ] . k u n d e n v o r n a m e   =   v o r n a m e ;    
 	 m o t o r a e d e r [ m o d e l l N r ] . k u n d e n n a c h n a m e   =   n a c h n a m e ;  
 	 m o t o r a e d e r [ m o d e l l N r ] . s t a t u s   =   " r e s e r v i e r t " ;  
  
 	 R e s e r v i e r u n g   * r ;  
 	 r   =   n e w   R e s e r v i e r u n g ( k u n ,   m o t o r a e d e r [ m o d e l l N r ] ) ;  
 	 t h i s - > r e s e r v i e r u n g e n . p u s h _ b a c k ( * r ) ;  
 }  
  
 / * G i b t   a l l e   a k t u e l l e n   R e s e r v i e r u n g e n   a u s . * /  
 v o i d   R e s e r v i e r u n g s L i s t e : : p r i n t R e s e r v i e r u n g e n ( )  
 {  
 	 i n t   i   =   0 ;  
 	 f o r   ( R e s e r v i e r u n g   r   :   r e s e r v i e r u n g e n )   {  
 	 	 s t d : : c o u t   < <   " * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * "   < <   e n d l  
 	 	 	 < <   + + i   < <   " .   R e s e r v i e r u n g : "   < <   e n d l    
 	 	 	 < <   r ;  
 	 }  
 	 s t d : : c o u t   < <   e n d l ;  
 }  
  
 / * G i b t   a l l e   r e s e r v i e r u n g e n   d e r   u e b e r g e b e n e n   L i s t e   a u s .   W i r d   f u e r   d a s   A u s g e b e n  
 d e r   g e s p e i c h e r t e n   D a t e i   b e n o e t i g t . * /  
 v o i d   R e s e r v i e r u n g s L i s t e : : p r i n t R e s e r v i e r u n g e n ( l i s t < R e s e r v i e r u n g >   r e s e r )  
 {  
 	 i n t   i   =   0 ;  
 	 f o r   ( R e s e r v i e r u n g   r   :   r e s e r )   {  
 	 	 s t d : : c o u t   < <   " * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * "   < <   e n d l  
 	 	 	 < <   + + i   < <   " .   R e s e r v i e r u n g : "   < <   e n d l  
 	 	 	 < <   r ;  
 	 }  
 	 s t d : : c o u t   < <   e n d l ;  
 }  
  
 v o i d   R e s e r v i e r u n g s L i s t e : : r e m o v e R e s e r v i e r u n g ( )  
 {  
  
 	 s t d : : c o u t   < <   " M o e c h t e n   S i e   a n h a n d   d e s   M o t o r r a d s   ( 1 )   o d e r   a n h a n d   d e s   K u n d e n   ( 2 )   d i e   R e s e r v i e r u n g   e n t f e r n e n ? "   < <   e n d l  
 	 	 < <   " B i t t e   g e b e n   s i e   1   o d e r   2   e i n :   " ;  
 	 i n t   c ;  
 	 s t d : : c i n   > >   c ;  
 	 M o t o r r a d   t m p M ;  
 	 K u n d e   t m p K ;  
 	 s t r i n g   v o r n a m e ,   n a c h n a m e ;  
 	 i n t   m o t o r ;  
 	 s w i t c h   ( c )  
 	 {  
 	 c a s e   1 :  
 	 	 s t d : : c o u t   < <   e n d l   < <   " F u e r   w e l c h e s   M o t o r r a d   m o e c h t e n   S i e   d i e   R e s e r v i e r u n g s   s t o r n i e r e n ? "  
 	 	 	 < <   e n d l   < <   " 1 :   S u z u k i , "   < <   e n d l  
 	 	 	 < <   " 2 :   H o n d a , "   < <   e n d l  
 	 	 	 < <   " 3 :   B M W , "   < <   e n d l  
 	 	 	 < <   " 4 :   K a w a s a k i "   < <   e n d l ;  
 	 	 s t d : : c i n   > >   m o t o r ;  
 	 	 t m p M   =   m o t o r a e d e r [ m o t o r ] ;  
 	 	 t m p K   =   k u n d e n L i s t e - > g e t K u n d e ( t m p M . k u n d e n n a c h n a m e ,   t m p M . k u n d e n v o r n a m e ) ;  
 	 	 b r e a k ;  
 	 c a s e   2 :  
 	 	 s t d : : c o u t   < <   " F u e r   w e l c h e n   K u n d e n   m o e c h t e n   S i e   e i n e   R e s e r v i e r u n g   e n t f e r n e n ? "   < <   e n d l   < <   " V o r n a m e :   " ;  
 	 	 s t d : : c i n   > >   v o r n a m e ;  
 	 	 s t d : : c o u t   < <   e n d l   < <   " N a c h n a m e :   " ;  
 	 	 s t d : : c i n   > >   n a c h n a m e ;  
 	 	 t m p K   =   k u n d e n L i s t e - > g e t K u n d e ( n a c h n a m e ,   v o r n a m e ) ;  
 	 	 f o r   ( i n t   i   =   0 ;   i   <   4 ;   i + + )   {  
 	 	 	 i f   ( m o t o r a e d e r [ i ] . k u n d e n n a c h n a m e   = =   n a c h n a m e   & &   m o t o r a e d e r [ i ] . k u n d e n v o r n a m e   = =   v o r n a m e )   {  
 	 	 	 	 m o t o r   =   i ;  
 	 	 	 	 i   =   4 ;   / / l e a v e s   f o r   l o o p  
 	 	 	 }   / / i f  
 	 	 }   / / f o r  
 	 d e f a u l t :  
 	 	 b r e a k ;  
 	 }  
 	 k u n d e n L i s t e - > r e m o v e K u n d e ( t m p K ) ;  
 	 t m p K . s t a t u s   =   " " ;  
 	 t m p K . a s s o z i i e r t e s M o t o r r a d   =   * ( n e w   M o t o r r a d ) ;  
 	 k u n d e n L i s t e - > a d d K u n d e ( t m p K ) ;  
 	 m o t o r a e d e r [ m o t o r ] . k u n d e n n a c h n a m e   =   " " ;  
 	 m o t o r a e d e r [ m o t o r ] . k u n d e n v o r n a m e   =   " " ;  
 	 m o t o r a e d e r [ m o t o r ] . s t a t u s   =   " g e l a g e r t " ;  
 	 f o r   ( R e s e r v i e r u n g   r   :   t h i s - > r e s e r v i e r u n g e n )  
 	 {  
 	 	 i f   ( r . k u n d e . n a m e   = =   t m p K . n a m e   & &   r . k u n d e . v o r n a m e   = =   t m p K . v o r n a m e )  
 	 	 {  
 	 	 	 t h i s - > r e s e r v i e r u n g e n . r e m o v e ( r ) ;  
 	 	 	 s t d : : c o u t   < <   " D i e   R e s e r v i e r u n g   w u r d e   e r f o l g r e i c h   e n t f e r n t ! "   < <   e n d l   < <   e n d l ;  
 	 	 	 r e t u r n ;   / / e n d s   f o r   l o o p   a n d   e x i t s   f u n c t i o n  
 	 	 }   / /   i f  
 	 }   / /   f o r    
 }  
  
 v o i d   R e s e r v i e r u n g s L i s t e : : a u s l a g e r n D e r R e s e r v i e r u n g ( )  
 {  
 	  
 	 K u n d e   t m p K ;  
 	 s t r i n g   v o r n a m e ,   n a c h n a m e ;  
 	 i n t   m o t o r ;  
 	 s t d : : c o u t   < <   " F u e r   w e l c h e n   K u n d e n   m o e c h t e n   S i e   e i n   M o t o r r a d   a u s l a g e r n   /   v e r l e i h e n ? "   < <   e n d l   < <   " V o r n a m e :   " ;  
 	 s t d : : c i n   > >   v o r n a m e ;  
 	 s t d : : c o u t   < <   e n d l   < <   " N a c h n a m e :   " ;  
 	 s t d : : c i n   > >   n a c h n a m e ;  
 	  
 	 f o r   ( R e s e r v i e r u n g   r e s   :   r e s e r v i e r u n g e n )   {   / / U e b e r p r u e f u n g ,   o b   e i n e   R e s e r v i e r u n g   u n t e r   d e m   N a m e n   v o r l i e g t .  
 	 	 i f   ( r e s . k u n d e . n a m e   = =   n a c h n a m e   & &   r e s . k u n d e . v o r n a m e   = =   v o r n a m e )   {  
 	 	 	 t m p K   =   r e s . k u n d e ;  
 	 	 }   / / i f  
 	 }   / / f o r  
 	 i f   ( t m p K . n a m e   = =   "   " )   {   / / f a l l s   k e i n e   R e s e r v i e r u n g   v o r l i e g t ,   b e e n d e t   s i c h   d i e   F u n k t i o n  
 	 	 c o u t   < <   e n d l   < <   " L e i d e r   l i e g t   k e i n e   R e s e r v i e r u n g   f u e r   d i e s e n   K u n d e n   v o r .   B i t t e   v e r s u c h e n   S i e   e s   e r n e u t . "   < <   e n d l ;  
 	 	 r e t u r n ;  
 	 }   / / i f  
 	 f o r   ( i n t   i   =   0 ;   i   <   4 ;   i + + )   {  
 	 	 i f   ( m o t o r a e d e r [ i ] . k u n d e n n a c h n a m e   = =   n a c h n a m e   & &   m o t o r a e d e r [ i ] . k u n d e n v o r n a m e   = =   v o r n a m e )   {  
 	 	 	 m o t o r   =   i ;  
 	 	 	 i   =   4 ;   / / l e a v e s   f o r   l o o p  
 	 	 }   / / i f  
 	 }   / / f o r  
 	 t m p K . s t a t u s   =   " a u s g e l i e h e n " ;  
 	 t m p K . a s s o z i i e r t e s M o t o r r a d   =   m o t o r a e d e r [ m o t o r ] ;  
 	 m o t o r a e d e r [ m o t o r ] . s t a t u s   =   " a u s g e l i e h e n " ;  
 	 k u n d e n L i s t e - > a e n d e r K u n d e n S t a t u s ( t m p K ) ;  
  
 	 f o r   ( R e s e r v i e r u n g   r   :   t h i s - > r e s e r v i e r u n g e n )  
 	 {  
 	 	 i f   ( r . k u n d e . n a m e   = =   t m p K . n a m e   & &   r . k u n d e . v o r n a m e   = =   t m p K . v o r n a m e )  
 	 	 {  
 	 	 	 t h i s - > r e s e r v i e r u n g e n . r e m o v e ( r ) ;  
 	 	 	 s t d : : c o u t   < <   e n d l   < <   " D i e   R e s e r v i e r u n g   w u r d e   e r f o l g r e i c h   e n t f e r n t ,   d a   d a s   M o t o r r a d   a u s g e l a g e r t   w u r d e ! "   < <   e n d l   < <   e n d l ;  
 	 	 	 r e t u r n ;  
 	 	 }   / /   i f  
 	 }   / /   f o r    
 }  
  
 / * S p e i c h e r t   d i e   a k t u e l l e n   r e s e r v i e r u n g e n   i n   d e r   D a t e i   " r e s e r v i e r u n g e n . t x t " * /  
 v o i d   R e s e r v i e r u n g s L i s t e : : s p e i c h e r n ( )  
 {  
 	 s t r i n g   d a t e i   =   " r e s e r v i e r u n g e n " ;  
 	 d a t e i   + =   " . t x t " ;  
 	 o f s t r e a m   e i n l e s e n D a t e i ( d a t e i . c _ s t r ( ) ,   i o s : : a p p ) ;   / / f u e g t   a n   r e s e r v i e r u n g e n . t x t   a n .   F a l l s   d i e   D a t e i   n i c h t   e x i s t i e r t ,   w i r d   s i e   a n g e l e g t .  
 	 f o r   ( R e s e r v i e r u n g   r   :   r e s e r v i e r u n g e n )   {  
 	 	 e i n l e s e n D a t e i   < <   r   < <   e n d l ;  
 	 }  
 	 e i n l e s e n D a t e i . c l o s e ( ) ;  
 }  
  
 / * G i b t   d e n   I n h a l t   d e r   r e s e r v i e r u n g e n . t x t   a u s * /  
 v o i d   R e s e r v i e r u n g s L i s t e : : d a t e i L a d e n ( )  
 {  
 	 l i s t < R e s e r v i e r u n g >   t m p L i s t e ;  
 	 s t r i n g   d a t e i   =   " r e s e r v i e r u n g e n " ;  
 	 d a t e i   + =   " . t x t " ;  
 	 i f s t r e a m   a u s l e s e n D a t e i ( d a t e i . c _ s t r ( ) ) ;  
 	 i f   ( ! a u s l e s e n D a t e i . g o o d ( ) )   {  
 	 	 c o u t   < <   " L e i d e r   l i e g t   k e i n e   g e s p e i c h e r t e   L i s t e   v o r . "   < <   e n d l ;  
 	 	 a u s l e s e n D a t e i . c l o s e ( ) ;  
 	 	 r e t u r n ;   / / b e e n d e t   d i e   F u n k t i o n ,   f a l l s   k e i n e   r e s e r v i e r u n g e n . t x t   v o r l i e g t .  
 	 }   / / i f  
  
 	 s t r i n g   n a c h n a m e ,   v o r n a m e ,   t e l e f o n ,   m o t o r r a d M o d e l l ,   s t r a s s e ,   o r t ,   f u e h r e r ;  
 	 i n t   g e b J a h r ,   h a u s n u m m e r ,   p l z ;  
  
 	 / / g e t r e n n t   d u r c h   L e e r z e i c h e n ,   w e r d e m   d i e   V a r i a b l e n   e i n e r   Z e i l e   a u s g e l e s e n .   D i e s   p a s s i e r t   Z e i l e n w e i s e .  
 	 w h i l e   ( a u s l e s e n D a t e i   > >   n a c h n a m e   > >   v o r n a m e   > >   g e b J a h r   > >   t e l e f o n    
 	 	 > >   f u e h r e r   > >   m o t o r r a d M o d e l l   > >   s t r a s s e   > >   h a u s n u m m e r   > >   p l z   > >   o r t )   / /   D a t e i   m i t a r b e i t e r . t x t   a u s l e s e n  
 	 {   / /   J e d e n   S p a l t e n w e r t   e i n e r   Z e i l e   i n   d i e   V a r i a b l e   s p e i c h e r n  
 	 	 b o o l   f u e h r e r s c h e i n   =   ( f u e h r e r   = =   " J a " ) ;  
 	 	 A d r e s s e   * a d r e s s e   =   n e w   A d r e s s e ( s t r a s s e ,   h a u s n u m m e r ,   p l z ,   o r t ) ;  
 	 	 K u n d e   * k   =   n e w   K u n d e ( n a c h n a m e ,   v o r n a m e ,   t e l e f o n ,   g e b J a h r ,   * a d r e s s e ,   f u e h r e r s c h e i n ) ;  
 	 	 k - > s t a t u s   =   " r e s e r v i e r t " ;  
 	 	 f o r   ( i n t   i   =   0 ;   i   <   4 ;   i + + )   {  
 	 	 	 i f   ( m o t o r a e d e r [ i ] . m o d e l l   = =   m o t o r r a d M o d e l l )   {  
 	 	 	 	 k - > a s s o z i i e r t e s M o t o r r a d   =   m o t o r a e d e r [ i ] ;  
 	 	 	 	 R e s e r v i e r u n g   * r   =   n e w   R e s e r v i e r u n g ( * k ,   m o t o r a e d e r [ i ] ) ;  
 	 	 	 	 t m p L i s t e . p u s h _ b a c k ( * r ) ;  
 	 	 	 	 i   =   4 ;   / / e n d s   t h e   f o r   l o o p  
 	 	 	 }   / / i f  
 	 	 }   / / f o r  
 	 	 p r i n t R e s e r v i e r u n g e n ( t m p L i s t e ) ;  
 	 }   / / w h i l e  
 }   / / d a t e i L a d e n  
  
 / * G i b t   d e n   N a m e n ,   d e n   S t a t u s   u n d   g g f .   d e n   N a m e n   d e s   K u n d e n   i m   F a l l e   e i n e r   R e s e r v i e r u n g   /   V e r l e i h u n g ,   a l l e r   M o t o r r a e d e r   a u s * /  
 v o i d   R e s e r v i e r u n g s L i s t e : : p r i n t M o t o r r a e d e r ( )  
 {  
 	 f o r   ( i n t   i   =   0 ;   i   <   4 ;   i + + )   {  
 	 	 s t d : : c o u t   < <   m o t o r a e d e r [ i ]   < <   e n d l ;  
 	 }  
 }  
  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / /   N E X T   C L A S S   :   M O T O R R A D V E R M I E T U N G  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / * S t e l l t   d a s   M e n u e   d e r   A n w e n d u n g   d a r * /  
 c l a s s   M o t o r r a d v e r m i e t u n g  
 {  
 p u b l i c :  
 	 M o t o r r a d v e r m i e t u n g ( )   :   r e s e r v i e r u n g e n ( n e w   R e s e r v i e r u n g s L i s t e ( & k u n d e n ) )   { }  
 	 v o i d   i n i t ( ) ;  
 p r i v a t e :  
 	 v o i d   m a i n M e n u ( ) ;  
 	 b o o l   i n p u t V a l i d a t i o n ( ) ;  
 	 v o i d   a d d K u n d e ( ) ;  
 	 v o i d   r e m o v e K u n d e ( ) ;  
 	 K u n d e n L i s t e   k u n d e n ;  
 	 R e s e r v i e r u n g s L i s t e   * r e s e r v i e r u n g e n ;  
 } ;  
  
 / *  
 E n g l i s c h   a u s   a n d e r e n   E A s .  
 p r o v i d e s   t h e   u s e r   w i t h   t h e   p o s s i b i l i t y   t o   v a l i d a t e   h i s   i n p u t .   I f   t h e   u s e r   e n t e r s   ' n ' ,   " f a l s e "   w i l l   b e   r e t u r n e d  
 I f   t h e   u s e r   e n t e r s   ' j ' ,   " t r u e "   w i l l   b e   r e t u r n e d .   O t h e r w i s e   t h e   u s e r   w i l l   b e   p r o m t e d   t o   e n t e r   a   c o r r e c t   c h a r .  
 * /  
 b o o l   M o t o r r a d v e r m i e t u n g : : i n p u t V a l i d a t i o n ( )  
 {  
 	 c h a r   c h e c k ;  
 	 s t d : : c i n   > >   c h e c k ;  
 	 b o o l   l e a v e Q u e s t i o n   =   0 ;  
 	 w h i l e   ( ! l e a v e Q u e s t i o n )  
 	 {  
 	 	 s w i t c h   ( c h e c k )  
 	 	 {  
 	 	 c a s e   ' j ' :  
 	 	 c a s e   ' J ' :  
 	 	 	 r e t u r n   t r u e ;  
 	 	 c a s e   ' n ' :  
 	 	 c a s e   ' N ' :  
 	 	 	 r e t u r n   f a l s e ;  
 	 	 d e f a u l t :  
 	 	 	 s t d : : c o u t   < <   " B i t t e   g e b e n   s i e   j   f u e r   j a ,   o d e r   n   f u e r   n e i n   e i n . "   < <   e n d l ;  
 	 	 	 c o n t i n u e ;  
 	 	 }   / / s w i t c h   c a s e   f o r   u s e r   i n p u t   o f   j / n  
 	 }   / / w h i l e  
 }   / / i n p u t V a l i d a t i o n  
  
 / * I n t e r a k t i o n   m i t   d e m   K u n d e n ,   u m   e i n e n   K u n d e n   h i n z u z u f u e g e n * /  
 v o i d   M o t o r r a d v e r m i e t u n g : : a d d K u n d e ( )  
 {  
 	 w h i l e   ( t r u e )   / / w i l l   b e   l e f t ,   i f   t h e   u s e r   d o e s n ' t   w a n t   t o   a d d   m o r e   i n v o i c e s  
 	 {  
 	 	 s t d : : c o u t   < <   " B i t t e   g e b e n   s i e   d i e   K u n d e n d a t e n   e i n "   < <   e n d l ;  
 	 	 k u n d e n . a d d K u n d e ( ) ;  
 	 	 s t d : : c o u t   < <   e n d l   < <   " M o e c h t e n   s i e   w e i t e r e   K u n d e n   h i n z u f u e g e n ?   ( j / n ) "   < <   e n d l ;  
 	 	 i f   ( ! i n p u t V a l i d a t i o n ( ) )  
 	 	 {  
 	 	 	 r e t u r n ;   / / l e a v e s   t h e   w h i l e   l o o p  
 	 	 }   / / i f  
 	 }   / /   w h i l e  
 }   / / a d d K u n d e  
  
 / * I n t e r a k t i o n   m i t   d e m   K u n d e n ,   u m   e i n e n   K u n d e n   z u   l o e s c h e n * /  
 v o i d   M o t o r r a d v e r m i e t u n g : : r e m o v e K u n d e ( )  
 {  
 	 s t r i n g   n ,   v ;  
 	 s t d : : c o u t   < <   " W i e   l a u t e t   d e r   V o r n a m e   d e s   K u n d e n ,   d e n   s i e   l o e s c h e n   m o e c h t e n ? "   < <   e n d l ;  
 	 s t d : : c i n   > >   v ;  
 	 s t d : : c o u t   < <   " W i e   l a u t e t   d e r   N a c h n a m e ? "   < <   e n d l ;  
 	 s t d : : c i n   > >   n ;  
 	 k u n d e n . r e m o v e K u n d e ( n ,   v ) ;  
 }  
  
 / * i n i t i l i z e s   t h e   u s e r i n t e r a c t i o n * /  
 v o i d   M o t o r r a d v e r m i e t u n g : : i n i t ( )  
 {  
 	 s t d : : c o u t   < <   " W i l l k o m m e n   i n   d e m   M o t o r r a d v e r m i e t u n g s   P r o g r a m m "   < <   e n d l   < <   e n d l ;  
 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 m a i n M e n u ( ) ;  
 }  
  
 / * r e p r e s e n t s   a   l o o p   i n   w h i c h   t h e   u s e r   c a n   a c c e s s   v a r i o u s   f u n c t i o n s   o f   t h e   M o t o r r a d v e r m i e t u n g * /  
 v o i d   M o t o r r a d v e r m i e t u n g : : m a i n M e n u ( )  
 {  
 	 b o o l   l o o p   =   1 ;  
 	 w h i l e   ( l o o p )   {  
 	 	 s t d : : c o u t   < <   e n d l   < <   " S i e   b e f i n d e n   s i c h   n u n   i m   H a u p t m e n u e "   < <   e n d l   < <   e n d l   < <   " S i e   h a b e n   f o l g e n d e   F u n k t i o n e n   z u r   V e r f u e g u n g :   "   < <   e n d l   < <   e n d l  
 	 	 	 < <   " 1 :     K u n d e n   e r f a s s e n . "   < <   e n d l  
 	 	 	 < <   " 2 :     A l l e   b e k a n n t e n   K u n d e n   a n z e i g e n "   < <   e n d l  
 	 	 	 < <   " 3 .     K u n d e   l o e s c h e n "   < <   e n d l  
 	 	 	 < <   " 4 .     I n f o r m a t i o n e n   u e b e r   a l l e   M o t o r r a e d e r   a u s g e b e n "   < <   e n d l  
 	 	 	 < <   " 5 :     R e s e r v i e r u n g   a n l e g e n "   < <   e n d l  
 	 	 	 < <   " 6 :     A l l e   R e s e r v i e r u n g e n   a u f l i s t e n "   < <   e n d l  
 	 	 	 < <   " 7 :     R e s e r v i e r u n g   e n t f e r n e n "   < <   e n d l  
 	 	 	 < <   " 8 .     M o t o r r a d   a u s l a g e r n "   < <   e n d l  
 	 	 	 < <   " 9 .     A k t u e l l e   R e s e r v i e r u n g e n   s p e i c h e r n "   < <   e n d l  
 	 	 	 < <   " 1 0 .   G e s p e i c h e r t e   R e s e r v i e r u n g e n   l a d e n "   < <   e n d l  
 	 	 	 < <   " 1 1 .   P r o g r a m m e n d e "   < <   e n d l   < <   e n d l  
 	 	 	 < <   " Z u m   A u f r u f e n   e i n e r   F u n k t i o n e n   g e b e n   s i e   b i t t e   n u n   d i e   d a v o r s t e h e n d e   Z i f f e r   e i n :   "   < <   e n d l ;  
 	 	 i n t   e i n g a b e ;   / / t a k e s   v a l u e s   o f   1 - 6 ,   r e p r e s e n t i g   f u n c t i o n   c a l l s   i n   t h e   s w i t c h   c a s e   s t a t e m e n t  
 	 	 s t d : : c i n   > >   e i n g a b e ;  
 	 	 s w i t c h   ( e i n g a b e )  
 	 	 {  
 	 	 c a s e   1 :  
 	 	 	 a d d K u n d e ( ) ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 	 	 c o n t i n u e ;   / / s t a r t s   t h e   w h i l e   l o o p   a n e w   t o   g i v e   t h e   u s e r   t h e   p o s s i b i l i t y   t o   c h a i n   c o m m a n d s .  
 	 	 c a s e   2 :  
 	 	 	 k u n d e n . p r i n t K u n d e n ( ) ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 	 	 c o n t i n u e ;  
 	 	 c a s e   3 :  
 	 	 	 r e m o v e K u n d e ( ) ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 	 	 c o n t i n u e ;  
 	 	 c a s e   4 :  
 	 	 	 r e s e r v i e r u n g e n - > p r i n t M o t o r r a e d e r ( ) ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 	 	 c o n t i n u e ;  
 	 	 c a s e   5 :  
 	 	 	 r e s e r v i e r u n g e n - > a d d R e s e r v i e r u n g ( ) ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 	 	 c o n t i n u e ;  
 	 	 c a s e   6 :  
 	 	 	 r e s e r v i e r u n g e n - > p r i n t R e s e r v i e r u n g e n ( ) ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 	 	 c o n t i n u e ;  
 	 	 c a s e   7 :  
 	 	 	 r e s e r v i e r u n g e n - > r e m o v e R e s e r v i e r u n g ( ) ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 	 	 c o n t i n u e ;  
 	 	 c a s e   8 :  
 	 	 	 r e s e r v i e r u n g e n - > a u s l a g e r n D e r R e s e r v i e r u n g ( ) ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 	 	 c o n t i n u e ;  
 	 	 c a s e   9 :  
 	 	 	 r e s e r v i e r u n g e n - > s p e i c h e r n ( ) ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 	 	 c o n t i n u e ;  
 	 	 c a s e   1 0 :  
 	 	 	 r e s e r v i e r u n g e n - > d a t e i L a d e n ( ) ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 	 	 c o n t i n u e ;  
 	 	 c a s e   1 1 :   / / l e a v e s   t h e   l o o p   a n d   t h e r e f o r e   e x i t s   t h e   p r o g r a m  
 	 	 	 l o o p   =   0 ;  
 	 	 	 b r e a k ;   / / b r e a k s   t h e   s w i t c h   c a s e s  
 	 	 d e f a u l t :  
 	 	 	 s t d : : c o u t   < <   e n d l   < <   " B i t t e   g e b e n   s i e   e i n e   d e r   z u l a e s s i g e n   N u m m e r n   e i n . "   < <   e n d l ;  
 	 	 	 s t d : : s y s t e m ( " p a u s e " ) ;   / / p a u s e s   t h e   s t d : : s y s t e m   t o   g i v e   t h e   u s e r   t i m e   t o   p r o c e s s   t h e   i n f o r m a t i o n  
 	 	 }   / / s w i t c h  
 	 	  
 	 }   / / w h i l e ,   l o o p s   m e n u   u n t i l   u s e r   i n p u t s   1 1   t o   c l o s e   t h e   p r o g r a m .  
 }   / / m a i n M e n u  
  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 / /   M A I N  
 / / * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  
 i n t   m a i n ( )  
 {  
 	 M o t o r r a d v e r m i e t u n g   m ;  
 	 m . i n i t ( ) ;   / / s t a r t s   t h e   i n p u t   l o o p  
 	 s t d : : s y s t e m ( " p a u s e " ) ;  
 	 r e t u r n   1 ;  
  
 } 